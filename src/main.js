import Vue from 'vue'
import DesignSystem from 'components-design-system'
import 'components-design-system/dist/system/system.css'
import vueCustomElement from 'vue-custom-element'
import 'document-register-element';

Vue.use(DesignSystem);
Vue.use(vueCustomElement);
Vue.config.productionTip = false;

import cdsButtons from './components/buttons.vue'
import cdsAlerts from './components/alerts.vue'

Vue.customElement('wcds-button', cdsButtons);
Vue.customElement('wcds-alert', cdsAlerts);